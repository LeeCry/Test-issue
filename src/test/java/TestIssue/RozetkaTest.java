package TestIssue;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class RozetkaTest extends BaseTest {

    @BeforeMethod
    public void beforeEachTest() throws IOException {
        mainPage.init();
        categoryPage.init();
        subCategoryPage.init();
        searchOutputPage.init();
        mainPage.openWebsite("http://rozetka.com.ua/");
        maximize();
    }

    @Test
    public void topPricePhonesTest() {
        mainPage.selectPhoneTvElectronicsCategory();

        categoryPage.selectPhoneCategory();

        subCategoryPage.selectSmartPhoneCat();

        searchOutputPage.showMoreProduct(2);
        searchOutputPage.getNameAndPriceOfTopPriceDevices();

        Assert.assertTrue(searchOutputPage.checkSizeLowerThan(3));

    }
}
