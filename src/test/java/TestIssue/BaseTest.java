package TestIssue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;



public class BaseTest {

    WebDriver driver = new ChromeDriver();

    MainPagePO mainPage = new MainPagePO(driver);
    CategoryPO categoryPage = new CategoryPO(driver);
    SubCategoryPO subCategoryPage = new SubCategoryPO(driver);
    SearchOutputPO searchOutputPage = new SearchOutputPO(driver);

    public void maximize() {
        driver.manage().window().maximize();
    }

    @AfterClass
    public void postcondition() {
        driver.quit();
    }
}
