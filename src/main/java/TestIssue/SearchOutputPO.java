package TestIssue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchOutputPO extends BasePO {
    public WebDriver driver;

    public SearchOutputPO(WebDriver driver) {
        this.driver = driver;
    }

    public List<WebElement> getProductsRows() {
        return driver.findElements(parser.getObjectLocator("ProductSelector"));
    }

    public WebElement getProductRow(int index) {
        return getProductsRows().get(index);
    }

    public List<String> getNameAndPriceOfTopPriceDevices() {
        List<String> nameAndPrices = new ArrayList<>();

        for (int i = 0; i < getProductsRows().size(); i++) {
            try {
                if (getProductRow(i).findElement(parser.getObjectLocator("BestPriceSelector")).isDisplayed()) {
                    nameAndPrices.add("Product name: " + getProductRow(i).findElement(parser.getObjectLocator("NameOfProduct")).getText()
                            + " ; Product price: " + getProductRow(i).findElement(parser.getObjectLocator("PriceOfProduct")).getText() + "\n");
                }
            } catch (org.openqa.selenium.NoSuchElementException e) {

            }
        }
        System.out.println(nameAndPrices);
        return nameAndPrices;
    }

    public void showMoreProduct(Integer num) {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        for (int i = 0; i < num; i++) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(parser.getObjectLocator("ShowMoreBTN")));
            driver.findElement(parser.getObjectLocator("ShowMoreBTN")).click();
        }
    }

    public List<WebElement> getTopPriceProducts() {
        return driver.findElements(parser.getObjectLocator("BestPriceSelector"));
    }

    public Boolean checkSizeLowerThan(Integer size) {
        if (getTopPriceProducts().size() <= 3) {
            return true;
        } else {
            return false;
        }
    }
}
