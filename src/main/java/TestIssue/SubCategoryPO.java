package TestIssue;

import org.openqa.selenium.WebDriver;

public class SubCategoryPO extends BasePO {

    public WebDriver driver;

    public SubCategoryPO(WebDriver driver) {
        this.driver = driver;
    }

    public void selectSmartPhoneCat() {
        driver.findElement(parser.getObjectLocator("SmartPhoneSelector")).click();
    }
}
