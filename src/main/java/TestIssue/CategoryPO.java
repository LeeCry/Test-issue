package TestIssue;

import org.openqa.selenium.WebDriver;

public class CategoryPO extends BasePO {

    public WebDriver driver;

    public CategoryPO(WebDriver driver) {
        this.driver = driver;
    }

    public void selectPhoneCategory() {
        driver.findElement(parser.getObjectLocator("TelephoneCatSelector")).click();
    }
}
